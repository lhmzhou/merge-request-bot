package function

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/steveazz/merge-request-bot/internal/gitlab"
)

// actionOpen is the type of action that is used when a new merge request is
// opened.
const actionOpen = "open"

const (
	// gitlabEventHeader is the header sent for each webhook call made by
	// GitLab.
	gitlabEventHeader = "X-Gitlab-Event"
	// gitlabSecretToken is the header sent with the configured token
	// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#secret-token.
	gitlabSecretTokenHeader = "X-Gitlab-Token" //nolint:gosec
	// mergeRequestEventHeader the value from the gitlabEventHeader when it's a
	// merge request hook.
	mergeRequestEventHeader = "Merge Request Hook"
)

const webhookSecretTokenEnv = "GITLAB_WEBHOOK_SECRET_TOKEN" //nolint:gosec

// event represents an event pushed from a GitLab Merge Request webhook
// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#merge-request-events
type event struct {
	User             user             `json:"user"`
	ObjectAttributes objectAttributes `json:"object_attributes"`
}

type objectAttributes struct {
	ID     int    `json:"id"`
	Action string `json:"action"`
}

type user struct {
	Email string `json:"email"`
}

// Options are functions that can modify the configuration of this function.
type Options func(f *Function)

// Function has the configuration to execute the function with a single Handler.
type Function struct {
	gitlabClient gitlab.Client
	logger       logrus.FieldLogger
	token        string
}

// NewFunction creates a new HTTP function
func NewFunction(client gitlab.Client, logger logrus.FieldLogger, opts ...Options) *Function {
	if logger == nil {
		newLogger := logrus.New()
		newLogger.SetFormatter(&logrus.JSONFormatter{})
		logger = newLogger
	}

	f := &Function{
		gitlabClient: client,
		logger:       logger,
	}

	for _, opt := range opts {
		opt(f)
	}

	return f
}

// WithTokenFromEnv sets the token for the function from the environment
// variable available to the OS. This token is used to authenticating requests
// to make sure no one sends random payloads.
// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#secret-token
func WithTokenFromEnv() Options {
	return func(f *Function) {
		f.token = os.Getenv(webhookSecretTokenEnv)
	}
}

// Handler handles HTTP requests for the function.
func (f *Function) Handler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	err := validateHeader(r.Header)
	if err != nil {
		f.logger.WithError(err).Info("invalid event from header")
		http.Error(w, err.Error(), http.StatusBadRequest)

		return
	}

	err = validateToken(r.Header, f.token)
	if err != nil {
		f.logger.WithError(err).Info("invalid secret token")
		http.Error(w, err.Error(), http.StatusUnauthorized)

		return
	}

	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		f.logger.WithError(err).Error("reading request body")
		http.Error(w, "reading request body", http.StatusInternalServerError)

		return
	}

	mrEvent := event{}

	err = json.Unmarshal(b, &mrEvent)
	if err != nil {
		f.logger.WithError(err).Error("marshaling json body")
		http.Error(w, "marshaling json body", http.StatusBadRequest)

		return
	}

	w.Header().Set("Content-Type", "application/json")

	if mrEvent.ObjectAttributes.Action != actionOpen {
		f.logger.WithField("action", mrEvent.ObjectAttributes.Action).Info("ignoring event")
		w.WriteHeader(http.StatusAccepted)

		return
	}

	if isGitLabEmail(mrEvent.User) {
		f.logger.Info("ignoring event gitlab email")
		w.WriteHeader(http.StatusAccepted)

		return
	}

	err = f.gitlabClient.CreateMergeRequestNote(r.Context(), mrEvent.ObjectAttributes.ID)
	if err != nil {
		f.logger.WithError(err).Error("Failed to create merge request note")
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusOK)
}

func validateHeader(header http.Header) error {
	event := header.Get(gitlabEventHeader)
	if event != mergeRequestEventHeader {
		return fmt.Errorf("invalid event %s form %q header", event, gitlabEventHeader)
	}

	return nil
}

func validateToken(header http.Header, secretToken string) error {
	token := header.Get(gitlabSecretTokenHeader)
	if token != secretToken {
		return fmt.Errorf("invalid secret token %s", token)
	}

	return nil
}

func isGitLabEmail(u user) bool {
	return strings.Contains(u.Email, "@gitlab.com")
}
