package function

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/steveazz/merge-request-bot/internal/gitlab"
)

const validResp = `
{
	"data": {
		"createNote": {
			"note": {
				"id": "gid://gitlab/MergeRequest/322142700"
			}
		}	
	}
}
`

var validEventHeader = http.Header{
	gitlabEventHeader:       []string{mergeRequestEventHeader},
	gitlabSecretTokenHeader: []string{"valid"},
}

func TestHandler(t *testing.T) {
	tests := []struct {
		name           string
		header         http.Header
		body           string
		apiResp        string
		wantStatusCode int
	}{
		{
			name:           "close event",
			header:         validEventHeader,
			body:           "close.json",
			wantStatusCode: http.StatusAccepted,
		},
		{
			name:           "merge event",
			header:         validEventHeader,
			body:           "merge.json",
			wantStatusCode: http.StatusAccepted,
		},
		{
			name:           "reopen event",
			header:         validEventHeader,
			body:           "reopen.json",
			wantStatusCode: http.StatusAccepted,
		},
		{
			name:           "update event",
			header:         validEventHeader,
			body:           "reopen.json",
			wantStatusCode: http.StatusAccepted,
		},
		{
			name:           "open event internal",
			header:         validEventHeader,
			body:           "open.json",
			wantStatusCode: http.StatusAccepted,
		},
		{
			name:           "open event community contribution",
			header:         validEventHeader,
			body:           "open_community_contribution.json",
			apiResp:        validResp,
			wantStatusCode: http.StatusOK,
		},
		{
			name:           "open event community contribution invalid response from GitLab api",
			header:         validEventHeader,
			body:           "open_community_contribution.json",
			apiResp:        "",
			wantStatusCode: http.StatusInternalServerError,
		},
		{
			name:           "body is not json",
			header:         validEventHeader,
			body:           "not_json",
			wantStatusCode: http.StatusBadRequest,
		},
		{
			name: "invalid header send",
			header: http.Header{
				"X-Gitlab-Event": []string{"Note Hook"},
			},
			body:           "open.json",
			wantStatusCode: http.StatusBadRequest,
		},
		{
			name:           "no header sent",
			header:         nil,
			body:           "open.json",
			wantStatusCode: http.StatusBadRequest,
		},
		{
			name: "invalid secret token",
			header: http.Header{
				gitlabEventHeader:       []string{mergeRequestEventHeader},
				gitlabSecretTokenHeader: []string{"invalid"},
			},
			body:           "open.json",
			wantStatusCode: http.StatusUnauthorized,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mrEvent, err := os.Open(fmt.Sprintf("./testdata/%s", tt.body))
			require.NoError(t, err)

			req := httptest.NewRequest(http.MethodPost, "http://127.0.0.1", mrEvent)
			w := httptest.NewRecorder()
			req.Header = tt.header

			fn := NewFunction(*gitlabClient(t, tt.apiResp), nil)
			fn.token = "valid"
			fn.Handler(w, req)

			resp := w.Result()
			defer resp.Body.Close()

			require.Equal(t, tt.wantStatusCode, resp.StatusCode)
		})
	}
}

func gitlabClient(t *testing.T, resp string) *gitlab.Client {
	const token = "token"

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		gotToken := r.Header.Get("Authorization")
		assert.Equal(t, fmt.Sprintf("Bearer %s", token), gotToken)

		w.Header().Set("Content-Type", "application/json")
		_, err := fmt.Fprint(w, resp)
		require.NoError(t, err)
	}))

	return gitlab.NewClient(srv.URL, token, nil)
}

func TestWithTokenFromEnv(t *testing.T) {
	oldEnv := os.Getenv(webhookSecretTokenEnv)

	t.Cleanup(func() {
		_ = os.Setenv(webhookSecretTokenEnv, oldEnv)
	})

	err := os.Setenv(webhookSecretTokenEnv, t.Name())
	require.NoError(t, err)

	fn := &Function{}

	opt := WithTokenFromEnv()
	opt(fn)

	require.Equal(t, t.Name(), fn.token)
}
