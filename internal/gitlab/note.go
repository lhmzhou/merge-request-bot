package gitlab

import (
	"context"
	"fmt"

	"github.com/machinebox/graphql"
	"github.com/sirupsen/logrus"
)

const noteBody = `
Thank you for your contribution! :tada:

We always thrive to make the contribution experience as smooth as possible. To
try and make the review quicker would you mind if you take a look at:
- The merge request description explains clearly what the problem is being
  solved.
- What is the best way for a reviewer to test this merge request, is it possible
  to provide an example?
- Is the pipeline successful, do you need help with getting it green?
- Check if it follows our [Go
  guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
- Read our [contributing to GitLab
  Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/CONTRIBUTING.md#contribute-to-gitlab-runner)
  document.

---

We sometimes get a large number of community contributions, which we are so
grateful for but it might take us some time to review, and we have to
[prioritize reviews](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/CONTRIBUTING.md#how-we-prioritize-mrs-from-the-wider-community).` //nolint:lll

type createNotePayload struct {
	CreateNote struct {
		Note struct {
			ID     string `json:"id"`
			Author struct {
				Username string `json:"username"`
			} `json:"author"`
		} `json:"note"`
	} `json:"createNote"`
}

// Client is used to communicate with GitLab's API.
type Client struct {
	graphql graphql.Client
	token   string
	logger  logrus.FieldLogger
}

// NewClient creates a new client for the specified endpoint using the token for
// authorization. The token is personal access token that should have `api` as
// permissions, as explained in
// https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
func NewClient(api string, token string, logger logrus.FieldLogger) *Client {
	if logger == nil {
		newLogger := logrus.New()
		newLogger.SetFormatter(&logrus.JSONFormatter{})
		logger = newLogger
	}

	client := graphql.NewClient(api)
	client.Log = func(s string) {
		logger.Debug(s)
	}

	return &Client{
		graphql: *client,
		token:   token,
		logger:  logger,
	}
}

// CreateMergeRequestNote creates a note (comment) on the specified merge
// request. Any error returned from the API is retuned.
func (c *Client) CreateMergeRequestNote(ctx context.Context, id int) error {
	logger := c.logger.WithField("merge_request_id", id)

	req := graphql.NewRequest(`
mutation ($id: ID! $body: String!) {
  createNote(input: {noteableId: $id, body: $body}) {
    note {
      id
    }
  }
}
`)

	req.Var("id", fmt.Sprintf("gid://gitlab/MergeRequest/%d", id))
	req.Var("body", noteBody)
	req.Header.Add("Authorization", "Bearer "+c.token)

	var resp createNotePayload
	if err := c.graphql.Run(ctx, req, &resp); err != nil {
		return err
	}

	logger.Info("Created merge request note")

	return nil
}
